#scrapem-task.py - Automated HypeMachine.com Page Scraper
#V.1.2 - 7/13/13
#Developed by: Matthiew Morin (matt@withani.net)
#Based off of Farid Marwan Zakaria's HypeScraper
#Requires the Beautiful Soup 4 and Mutagen Libraries
#
#!!! This version has become obsolete due to the --quiet parameter added to scrapem.py !!!

import unicodedata
from time import time
import sys
import urllib2
import urllib
import json
import string
from bs4 import BeautifulSoup
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
import mutagen.id3

#Define Valid File Name Characters
validFileNameChars = "-_.() %s%s" % (string.ascii_letters, string.digits)

#Clean File Name
def cleanFileName(filename):
	cleanedFileName = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore')
	return ''.join(c for c in cleanedFileName if c in validFileNameChars)

class scrapeM:

	def __init__(self):
		pass
	
	def start(self):
		#Splash Screen & User Definitions
		print "#############################################"
		print "#                                  __  __   #"
		print "#    ___  ___ _ __ __ _ _ __   ___|  \/  |  #"
		print "#   / __|/ __| '__/ _` | '_ \ / _ | |\/| |  #"
		print "#   \__ | (__| | | (_| | |_) |  __| |  | |  #"
		print "#   |___/\___|_|  \__,_| .__/ \___|_|  |_|  #"
		print "#                      |_|                  #"
		print "#############################################"
		print "# Automatically download your favorite page #"
		print "#        or pages from HypeM.com.           #"
		print "#############################################"
		print ""
		print "Enter the page that you would like to download:"
		print "(I.E: latest, popular, username, etc)"
		scrapePage = raw_input("http://www.hypem.com/")
		numOfPages = raw_input("How many pages would you like to download: ")
		pagesInt = int(numOfPages)
		url = 'http://hypem.com/{}'.format(scrapePage)
		
		#Getting down to BizNez
		print "Commencing Operations..."
		print "Downloading songs from: ", url 
		
		#Loop for pages
		for i in range(1, pagesInt + 1):
			print "Checking Page {} of {}".format(i, pagesInt)
			
			#Get Cookie and HTML Data
			pageUrl = url + "/{}".format(i)
			html, cookie = self.getHtml(pageUrl)
			
			#Find Tracks on Page
			tracks = self.parseHtml(html)
			
			print "Found {} Songs".format(len(tracks))
			
			#Download Tracks on Page
			self.downloadSongs(tracks, cookie)
	
	#Get Cookie and HTML Data
	def getHtml(self, url):
		data = {'ax':1, 'ts': time()}
		encodedData = urllib.urlencode(data)
		full_url = url + "?{}".format(encodedData)
		request = urllib2.Request(full_url)
		response = urllib2.urlopen(request)
		cookie = response.headers.get('Set-Cookie')
		html = response.read()
		response.close()
		return html, cookie
		
	#Find Tracks on Page & Build Track List
	def parseHtml(self, html):
		trackList = []
		soup = BeautifulSoup(html)
		tracksInHtml = soup.find(id="displayList-data")
		if tracksInHtml is None:
			return trackList
		try:
			trackList = json.loads(tracksInHtml.text)
			return trackList[u'tracks']
		except ValueError:
			print "Invalid Input from HypeM"
			return trackList
		
	#The Real OG - Download, Rename, ID3
	def downloadSongs(self, tracks, cookie):	
		for track in tracks:
			
			#Get Track Specific Info
			key = track[u"key"]
			id = track [u"id"]
			artist = cleanFileName(track[u"artist"])
			title = cleanFileName(track[u"song"])
			type = track[u"type"]
			print "Downloading {} by {}".format(title, artist)
		
			if type is False:
				continue
			
			#Download and write to file
			try:
				serviceUrl = "http://hypem.com/serve/source/{}/{}".format(id, key)
				request = urllib2.Request(serviceUrl, "", {'Content-Type': 'application/json'})
				request.add_header('cookie', cookie)
				response = urllib2.urlopen(request)
				jSong = response.read()
				response.close()
				rawSong = json.loads(jSong)
				url = rawSong[u"url"]
			
				downResponse = urllib2.urlopen(url)
				fileName = "{} - {}.mp3".format(artist, title)
				mp3 = open(fileName, "wb")
				mp3.write(downResponse.read())
				mp3.close()
			except urllib2.HTTPError, e:
				print "Hypem.com Download URL Returned Error: " + str(e.code)
		
			except urllib2.URLError, e:
				print "Hypem.com Download URL Returned Error: " + str(e.reaseon)
		
			except Exception, e:
				print "Something Random Broke.  Error: " + str(e)
				
			#Set ID3 Data
			file = MP3(fileName, ID3=EasyID3)
			file['title'] = title
			file['artist'] = artist
			file.save()
			
			
#Main, Does Main Stuff
def main():
	scraper = scrapeM()
	scraper.start()

if __name__ == "__main__":
	main()

