#scrapem.py - Automated HypeMachine.com Page Scraper
#V.2.1 - 12/22/13
#Developed by: Matthiew Morin (matt@withani.net)
#Based off of Farid Marwan Zakaria's HypeScraper
#Requires the Beautiful Soup 4 and Mutagen Libraries
#
#!!! This version requires user input; for scheduled tasks use scrapem-task.py !!!

import platform
import unicodedata
from time import time
import sys
import urllib2
import urllib
import json
import string
from bs4 import BeautifulSoup
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
import mutagen.id3
import argparse

#Command Line Argument Handling
parser = argparse.ArgumentParser(description='scrapeM.py - Download your favorite page or pages from HypeM.com')
parser.add_argument("-q", "--quiet", help="Quiet Mode. Disables user input and feedback, good for running in a scheduled mode.", action="store_true")
args = parser.parse_args()

if args.quiet:
	programMode = 'quiet'
else:
	programMode = ''

#Get OS
os = platform.system()

#Define Valid File Name Characters
validFileNameChars = "-_.() %s%s" % (string.ascii_letters, string.digits)

#Clean File Name
def cleanFileName(filename):
	cleanedFileName = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore')
	return ''.join(c for c in cleanedFileName if c in validFileNameChars)

#Main ScrapeM Class
class scrapeM:

	def __init__(self):
		pass

	#Check if provided filePath is a directory (by checking if a trailing slash is present)
	#If there is no slash present, add one.  This may not be completely necessary on all
	#Systems but it helps with compatibility issues
	def filePathCheck(self, filePath):
		if os == 'Windows':
			if filePath[-1] != "\\":
				filePath = filePath + "\\"
				return filePath
		else:
			if filePath[-1] != "/":
				filePath = filePath + "/"
				return filePath
	
	def start(self):
	
		#If Quiet Mode is enabled (-q command line argument), run with these arguments
		if programMode == 'quiet':
			scrapePage = 'latest'
			pagesInt = 1
			filePath = ''
			
		#If Quiet Mode is not enabled, use user input
		else:
			#Splash Screen & User Definitions
			print "#############################################"
			print "#                                  __  __   #"
			print "#    ___  ___ _ __ __ _ _ __   ___|  \/  |  #"
			print "#   / __|/ __| '__/ _` | '_ \ / _ | |\/| |  #"
			print "#   \__ | (__| | | (_| | |_) |  __| |  | |  #"
			print "#   |___/\___|_|  \__,_| .__/ \___|_|  |_|  #"
			print "#                      |_|                  #"
			print "#############################################"
			print "# Automatically download your favorite page #"
			print "#        or pages from HypeM.com.           #"
			print "#############################################"
			print ""
			filePath = raw_input("Where would you like to save the files to: ")
			filePath = self.filePathCheck(filePath)
			print "Enter the page that you would like to download:"
			print "(I.E: latest, popular, username, etc)"
			scrapePage = raw_input("http://www.hypem.com/")
			numOfPages = raw_input("How many pages would you like to download: ")
			pagesInt = int(numOfPages)

		#Set actual URL to be used
		url = 'http://hypem.com/{}'.format(scrapePage)
		
		#Getting down to BizNez
		print "Commencing Operations..."
		print "Downloading songs from: ", url 
		
		#Loop for pages
		for i in range(1, pagesInt + 1):
			print "Checking Page {} of {}".format(i, pagesInt)
			
			#Get Cookie and HTML Data
			pageUrl = url + "/{}".format(i)
			html, cookie = self.getHtml(pageUrl)
			
			#Find Tracks on Page
			tracks = self.parseHtml(html)
			
			print "Found {} Songs".format(len(tracks))
			
			#Download Tracks on Page
			self.downloadSongs(tracks, cookie, filePath)
	
	#Get Cookie and HTML Data
	def getHtml(self, url):
		data = {'ax':1, 'ts': time()}
		encodedData = urllib.urlencode(data)
		full_url = url + "?{}".format(encodedData)
		request = urllib2.Request(full_url)
		response = urllib2.urlopen(request)
		cookie = response.headers.get('Set-Cookie')
		html = response.read()
		response.close()
		return html, cookie
		
	#Find Tracks on Page & Build Track List
	def parseHtml(self, html):
		trackList = []
		soup = BeautifulSoup(html)
		tracksInHtml = soup.find(id="displayList-data")
		if tracksInHtml is None:
			return trackList
		try:
			trackList = json.loads(tracksInHtml.text)
			return trackList[u'tracks']
		except ValueError:
			print "Invalid Input from HypeM"
			return trackList
		
	#The Real OG - Download, Rename, ID3
	def downloadSongs(self, tracks, cookie, filePath):	
		for track in tracks:
			
			#Get Track Specific Info
			key = track[u"key"]
			id = track [u"id"]
			artist = cleanFileName(track[u"artist"])
			title = cleanFileName(track[u"song"])
			type = track[u"type"]
			print "Downloading {} by {}".format(title, artist)
		
			if type is False:
				continue
			
			#Download and write to file
			try:
				serviceUrl = "http://hypem.com/serve/source/{}/{}".format(id, key)
				request = urllib2.Request(serviceUrl, "", {'Content-Type': 'application/json'})
				request.add_header('cookie', cookie)
				response = urllib2.urlopen(request)
				jSong = response.read()
				response.close()
				rawSong = json.loads(jSong)
				url = rawSong[u"url"]
			
				downResponse = urllib2.urlopen(url)
				fileName = "{}{} - {}.mp3".format(filePath, artist, title)
				with open(fileName, "wb") as mp3:
					mp3.write(downResponse.read())
					mp3.close()
			except urllib2.HTTPError, e:
				print "Hypem.com Download URL Returned Error: " + str(e.code)
		
			except urllib2.URLError, e:
				print "Hypem.com Download URL Returned Error: " + str(e.reaseon)
		
			except Exception, e:
				print "Something Random Broke.  Error: " + str(e) + " On Line:", sys.exc_info()[-1].tb_lineno
				
			#Set ID3 Data
			file = MP3(fileName, ID3=EasyID3)
			file['title'] = title
			file['artist'] = artist
			file.save()
			
			
#Main, Does Main Stuff
def main():
	scraper = scrapeM()
	scraper.start()

if __name__ == "__main__":
	main()

